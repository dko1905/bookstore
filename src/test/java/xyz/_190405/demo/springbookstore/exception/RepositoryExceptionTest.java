package xyz._190405.demo.springbookstore.exception;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RepositoryExceptionTest {
	@Test
	public void throwAndCheckMessage() {
		String message = "Hello 123";
		int errorCode = 123;

		try {
			throw new RepositoryException(message, errorCode);
		} catch (RepositoryException e) {
			assertEquals(message, e.getMessage());
			assertEquals(errorCode, e.getErrorCode());
		}
	}

	@Test
	public void throwAndCheckInner() {
		Exception innerException = new Exception("123");

		try {
			throw new RepositoryException("msg", 922, innerException);
		} catch (RepositoryException e) {
			assertEquals(innerException, e.getInnerException());
		}
	}
}
