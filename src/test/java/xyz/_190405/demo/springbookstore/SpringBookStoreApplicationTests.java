package xyz._190405.demo.springbookstore;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class SpringBookStoreApplicationTests {

	@Test
	void contextLoads() {
	}

}
