package xyz._190405.demo.springbookstore.repository;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.TestInstance.*;

import static org.junit.jupiter.api.Assertions.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import xyz._190405.demo.springbookstore.exception.RepositoryException;
import xyz._190405.demo.springbookstore.model.Author;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
@ActiveProfiles("test")
public class AuthorRepositoryTest {
	private static final Logger logger = LoggerFactory.getLogger(AuthorRepositoryTest.class);
	@Autowired
	private AuthorRepository authorRepository;
	@Autowired
	private DataSource dataSource;

	@BeforeEach @AfterAll
	public void clearDb() throws SQLException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM sc_author;")) {
			preparedStatement.execute();
		}
	}

	@Test
	public void addAndGet() throws RepositoryException {
		Author original = new Author(0, "full name here", 1.55);

		Author saved = authorRepository.add(original);

		assertNotNull(saved);
		assertEquals(original.getFullName(), saved.getFullName());
		assertTrue(Math.abs(original.getRating() - saved.getRating()) <= 0.0001);

		Author gotten = authorRepository.findById(saved.getId()).get();

		assertNotNull(gotten);
		assertEquals(saved.getId(), gotten.getId());
		assertEquals(saved.getFullName(), gotten.getFullName());
		assertTrue(Math.abs(saved.getRating() - gotten.getRating()) <= 0.0001);
	}

	@Test
	public void add3AndSearchByName() throws RepositoryException {
		ArrayList<Author> authors = new ArrayList<>();

		authors.add(new Author(0, "Aåææ #æø¤#$%'ss", 1.5));
		authors.add(new Author(0, "Aææ #æø¤#$%'ss", 1.5));
		authors.add(new Author(0, "Aå c#æø¤#$%'ss", 1.5));
		Author specialAuthor = new Author(0, "A';\"bææ #æø¤#$%'ss", 1.6);
		authors.add(specialAuthor);
		authors.add(new Author(0, "Aåææ c#æø¤#$%'ss", 1.5));
		authors.add(new Author(0, "_%%%", 1.5));

		for (int i = 0; i < authors.size(); ++i) {
			authors.set(i, authorRepository.add(authors.get(i)));
		}

		List<Author> foundAuthors = authorRepository.findByFullName("ø");
		assertEquals(5, foundAuthors.size());

		foundAuthors = authorRepository.findByFullName("b");
		assertEquals(1, foundAuthors.size());
		Author foundAuthor = foundAuthors.get(0);
		assertEquals(specialAuthor.getFullName(), foundAuthor.getFullName());
		assertTrue(Math.abs(specialAuthor.getRating() - foundAuthor.getRating()) <= 0.0001);

		foundAuthors = authorRepository.findByFullName("_%%%");
		assertEquals(1, foundAuthors.size());
	}

	@Test
	public void addAndUpdate() throws RepositoryException {
		Author author = new Author(0, "Daniel", 1.4);

		author = authorRepository.add(author);

		author.setFullName("Alex");

		Author updatedAuthor = authorRepository.update(author);

		assertEquals(author.getId(), updatedAuthor.getId());
		assertTrue(Math.abs(author.getRating() - updatedAuthor.getRating()) <= 0.0001);
		assertEquals(author.getFullName(), updatedAuthor.getFullName());

		updatedAuthor = authorRepository.findById(author.getId()).get();

		assertEquals(author.getId(), updatedAuthor.getId());
		assertTrue(Math.abs(author.getRating() - updatedAuthor.getRating()) <= 0.0001);
		assertEquals(author.getFullName(), updatedAuthor.getFullName());
	}

	@Test
	public void addAndGetAll() throws RepositoryException {
		ArrayList<Author> authors = new ArrayList<>();
		authors.add(authorRepository.add(new Author(0, "daniel", 1.3)));
		authors.add(authorRepository.add(new Author(0, "daniel1", 1.3)));
		authors.add(authorRepository.add(new Author(0, "daniel2", 1.3)));

		List<Author> foundAuthors = authorRepository.findAll();
		assertEquals(3, foundAuthors.size());

		for (final Author author : authors) {
			boolean found = false;
			for (final Author author1 : foundAuthors) {
				if (author1.getId() == author.getId()) found = true;
			}
			assertTrue(found);
		}
	}

	@Test
	public void updateWithInvalidAuthor() throws RepositoryException {
		Author author = authorRepository.add(new Author(0, "da", 69));

		boolean caught = false;
		try {
			author.setId(author.getId() + 1);
			authorRepository.update(author);
		} catch (RepositoryException e) {
			assertEquals(3, e.getErrorCode());
			caught = true;
		}
		assertTrue(caught);
	}

	@Test
	public void deleteWithInvalidAuthor() throws RepositoryException {
		boolean caught = false;
		try {
			authorRepository.deleteById(1);
		} catch (RepositoryException e) {
			assertEquals(3, e.getErrorCode());
			caught = true;
		}
		assertTrue(caught);
	}

	@Test
	public void addAndDelete() throws RepositoryException {
		Author author = authorRepository.add(new Author(
				0,
				"D",
				69
		));

		Author author1 = authorRepository.findById(author.getId()).get();
		assertNotNull(author1);
		assertEquals(author.getId(), author1.getId());

		authorRepository.deleteById(author.getId());

		assertTrue(authorRepository.findById(author.getId()).isEmpty());
	}
}
