package xyz._190405.demo.springbookstore.repository;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import xyz._190405.demo.springbookstore.exception.RepositoryException;
import xyz._190405.demo.springbookstore.model.User;

import javax.annotation.Nullable;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
public class UserRepositoryTest {
	static final Logger logger = LoggerFactory.getLogger(UserRepositoryTest.class);
	@Autowired
	UserRepository userRepository;
	@Autowired
	DataSource dataSource;

	@BeforeEach
	@AfterAll
	public void clearDb() throws SQLException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM sc_user;")) {
			preparedStatement.execute();
		}
	}

	private boolean userEqual(@Nullable User user, @Nullable User user1) {
		if (user == null && user1 == null) return true;
		else if (user == null || user1 == null) return false;
		else if (user.getId() == user1.getId() &&
				 user.getUsername().equals(user1.getUsername()) &&
				 user.getPassword().equals(user1.getPassword())) {
			return true;
		} else {
			return false;
		}
	}

	private void userAssert(@Nullable User user, @Nullable User user1) {
		if (user == null || user1 == null) return;
		assertEquals(user.getId(), user1.getId());
		assertEquals(user.getUsername(), user1.getUsername());
		assertEquals(user.getPassword(), user1.getPassword());
	}

	@Test
	public void add2ThenBreakUnique() throws RepositoryException {
		ArrayList<User> users = new ArrayList<>();

		users.add(new User(0, "daniel", "123", "USER"));
		users.add(new User(0, "daniel1", "123", "USER"));
		users.add(new User(0, "daniel2", "1234", "USER"));
		User breakUser = new User(0, "daniel", "1234", "USER");

		for (int n = 0; n < users.size(); n++) {
			users.set(n, userRepository.add(users.get(n)));
		}

		boolean thrown = false;
		try {
			userRepository.add(breakUser);
		} catch (RepositoryException e) {
			thrown = true;

			assertEquals(4, e.getErrorCode());
		}
		assertTrue(thrown);
	}

	@Test
	public void addAndGetAll() throws RepositoryException {
		ArrayList<User> users = new ArrayList<>();

		users.add(new User(0, "daniel", "123b", "USER"));
		users.add(new User(0, "daniel1", "12s3", "USER"));
		users.add(new User(0, "daniel2", "1234", "USER"));

		for (int n = 0; n < users.size(); n++) {
			users.set(n, userRepository.add(users.get(n)));
		}

		List<User> foundUsers = userRepository.findAll();

		for (final User user : users) {
			boolean found = false;
			for (final User user1 : foundUsers) {
				if (userEqual(user, user1)) {
					found = true;
				}
			}
			assertTrue(found);
		}
	}

	@Test
	public void emptyGetAll() throws RepositoryException {
		List<User> users = userRepository.findAll();
		assertEquals(0, users.size());
		assertTrue(userRepository.findByUsername("").isEmpty());
		assertTrue(userRepository.findById(1).isEmpty());
	}

	@Test
	public void addAndGetById() throws RepositoryException {
		ArrayList<User> users = new ArrayList<>();

		users.add(new User(0, "daniel", "123b", "USER"));
		users.add(new User(0, "daniel1", "12s3", "USER"));
		users.add(new User(0, "daniel2", "1234", "USER"));

		for (int n = 0; n < users.size(); n++) {
			users.set(n, userRepository.add(users.get(n)));
		}

		long totalId = 0;
		for (final User user : users) {
			User user1 = userRepository.findById(user.getId()).get();

			userAssert(user, user1);

			totalId += user.getId();
		}

		assertTrue(userRepository.findById(totalId).isEmpty());
	}

	@Test
	public void addAndGetByUsername() throws RepositoryException {
		ArrayList<User> users = new ArrayList<>();

		users.add(new User(0, "daniel", "123b", "USER"));
		users.add(new User(0, "daniel1", "12s3", "USER"));
		users.add(new User(0, "daniel2", "1234", "USER"));

		for (int n = 0; n < users.size(); n++) {
			users.set(n, userRepository.add(users.get(n)));
		}

		for (final User user : users) {
			User user1 = userRepository.findByUsername(user.getUsername()).get();

			userAssert(user, user1);
		}
	}

	@Test
	public void addAndUpdate() throws RepositoryException {
		ArrayList<User> users = new ArrayList<>();

		users.add(new User(0, "daniel", "123b", "USER"));
		users.add(new User(0, "daniel1", "12s3", "USER"));
		users.add(new User(0, "daniel2", "1234", "USER"));

		for (int n = 0; n < users.size(); n++) {
			users.set(n, userRepository.add(users.get(n)));
		}

		users.set(1, userRepository.update(new User(
				users.get(1).getId(),
				"123 123123",
				users.get(1).getUsername(),
				users.get(1).getRole()
		)));

		List<User> foundUsers = userRepository.findAll();

		long totalId = 0;
		for (final User user : users) {
			boolean found = false;
			for (final User user1 : foundUsers) {
				if (userEqual(user, user1)) {
					found = true;
				}
			}
			assertTrue(found);
			totalId += user.getId();
		}

		// User does not exist.
		boolean caught = false;
		try {
			userRepository.update(new User(
					totalId,
					"123",
					"123",
					"USER"
			));
		} catch (RepositoryException e) {
			if (e.getErrorCode() != 3) throw e;
			caught = true;
		}
		assertTrue(caught);
		// User unique is violated.
		caught = false;
		try {
			userRepository.update(new User(
					users.get(0).getId(),
					users.get(1).getUsername(),
					"123",
					"USER"
			));
		} catch (RepositoryException e) {
			if (e.getErrorCode() != 4) throw e;
			caught = true;
		}
		assertTrue(caught);
	}

	@Test
	public void addAndDelete() throws RepositoryException {
		ArrayList<User> users = new ArrayList<>();

		users.add(new User(0, "daniel", "123b", "USER"));
		users.add(new User(0, "daniel1", "12s3", "USER"));
		users.add(new User(0, "daniel2", "1234", "USER"));

		for (int n = 0; n < users.size(); n++) {
			users.set(n, userRepository.add(users.get(n)));
		}

		userRepository.deleteById(users.get(1).getId());
		users.remove(1);

		List<User> foundUsers = userRepository.findAll();
		long totalId = 0;

		for (final User user : users) {
			boolean found = false;
			for (final User user1 : foundUsers) {
				if (userEqual(user, user1)) {
					found = true;
				}
			}
			totalId += user.getId();
			assertTrue(found);
		}

		// Delete invalid user.
		boolean caught = false;
		try {
			userRepository.deleteById(totalId);
		} catch (RepositoryException e) {
			if (e.getErrorCode() != 3) throw e;
			caught = true;
		}
		assertTrue(caught);
	}
}
