package xyz._190405.demo.springbookstore.repository;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import xyz._190405.demo.springbookstore.exception.RepositoryException;
import xyz._190405.demo.springbookstore.model.Author;
import xyz._190405.demo.springbookstore.model.Book;

import javax.annotation.Nullable;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
public class BookRepositoryTest {
	private static final Logger logger = LoggerFactory.getLogger(AuthorRepositoryTest.class);
	@Autowired
	private BookRepository bookRepository;
	@Autowired
	private AuthorRepository authorRepository;
	@Autowired
	private DataSource dataSource;

	@BeforeEach
	@AfterAll
	public void clearDb() throws SQLException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM sc_book;");
			 PreparedStatement preparedStatement1 = connection.prepareStatement("DELETE FROM sc_author;")) {
			preparedStatement.execute();
			preparedStatement1.execute();
		}
	}

	@Test
	public void addAndGetOld() throws RepositoryException {
		Author author1 = authorRepository.add(new Author(0, "daniel f.", 1.d / 3.d));
		Book book1 = new Book(
				0,
				"The C Book",
				"subtitle",
				"description here",
				1.5,
				Instant.now(),
				new BigDecimal(123.42),
				author1.getId()
		);

		Book addedBook = bookRepository.add(book1);

		assertEquals(book1.getTitle(), addedBook.getTitle());
		assertEquals(book1.getSubtitle(), addedBook.getSubtitle());
		assertEquals(book1.getSubtitle(), addedBook.getSubtitle());
		assertEquals(book1.getDescription(), addedBook.getDescription());
		assertTrue(Math.abs(book1.getRating() - addedBook.getRating()) <= 0.0001);
		assertEquals(book1.getPublishDate(), addedBook.getPublishDate());

		book1 = addedBook;
		addedBook = bookRepository.findById(book1.getId()).get();

		assertEquals(book1.getTitle(), addedBook.getTitle());
		assertEquals(book1.getSubtitle(), addedBook.getSubtitle());
		assertEquals(book1.getSubtitle(), addedBook.getSubtitle());
		assertEquals(book1.getDescription(), addedBook.getDescription());
		assertTrue(Math.abs(book1.getRating() - addedBook.getRating()) <= 0.0001);
		assertEquals(book1.getPublishDate(), addedBook.getPublishDate());
	}

	@Test
	public void addAndFindByTitleOld() throws RepositoryException {
		ArrayList<Author> authors = new ArrayList<>();
		ArrayList<Book> books = new ArrayList<>();

		authors.add(new Author(
				0,
				"Daniel Flor",
				1.3
		));
		authors.add(new Author(
				0,
				"Alex Olsen",
				1.1
		));

		for (int n = 0; n < authors.size(); n++) {
			authors.set(n, authorRepository.add(authors.get(n)));
		}

		books.add(new Book(
				0,
				"Title here [[%$å",
				"sub",
				"desc",
				1.3,
				Instant.now(),
				new BigDecimal(1.3),
				authors.get(0).getId()
		));
		books.add(new Book(
				0,
				"Title here [[%$å;'ø",
				"sub",
				"desc",
				1.3,
				Instant.now(),
				new BigDecimal(1.3),
				authors.get(1).getId()
		));
		books.add(new Book(
				0,
				"Title here [[%$å",
				"sub",
				"desc",
				1.3,
				Instant.now(),
				new BigDecimal(1.3),
				authors.get(1).getId()
		));
		books.add(new Book(
				0,
				"Title here [[%$å",
				"sub",
				"desc",
				1.3,
				Instant.now(),
				new BigDecimal(1.3),
				authors.get(0).getId()
		));

		for (int n = 0; n < books.size(); n++) {
			books.set(n, bookRepository.add(books.get(n)));
		}

		List<Book> booksFound = bookRepository.findByTitle("å");
		assertEquals(books.size(), booksFound.size());

		booksFound = bookRepository.findByTitle("ø");
		assertEquals(1, booksFound.size());
		Book book1 = books.get(1);
		Book addedBook = booksFound.get(0);

		assertEquals(book1.getTitle(), addedBook.getTitle());
		assertEquals(book1.getSubtitle(), addedBook.getSubtitle());
		assertEquals(book1.getSubtitle(), addedBook.getSubtitle());
		assertEquals(book1.getDescription(), addedBook.getDescription());
		assertTrue(Math.abs(book1.getRating() - addedBook.getRating()) <= 0.0001);
		assertEquals(book1.getPublishDate(), addedBook.getPublishDate());
	}

	@Test
	public void addAndFindByWordOld() throws RepositoryException {
		ArrayList<Book> books = new ArrayList<>();
		Author author = authorRepository.add(new Author(0, "Daniel Florescu", 1.3333));

		books.add(new Book(
				0,
				"title z'_",
				"subtitle",
				"desc",
				1.3,
				Instant.now(),
				new BigDecimal(1.3),
				author.getId()
		));
		books.add(new Book(
				0,
				"title",
				"subtitle z'",
				"desc",
				1.3,
				Instant.now(),
				new BigDecimal(1.3),
				author.getId()
		));
		books.add(new Book(
				0,
				"title",
				"subtitle z'_",
				"desc",
				1.3,
				Instant.now(),
				new BigDecimal(1.3),
				author.getId()
		));
		books.add(new Book(
				0,
				"title",
				"subtitle",
				"desc z'_",
				1.3,
				Instant.now(),
				new BigDecimal(1.3),
				author.getId()
		));

		for (int n = 0; n < books.size(); n++) {
			books.set(n, bookRepository.add(books.get(n)));
		}

		List<Book> booksFound = bookRepository.findByWord("z'_");
		assertEquals(3, booksFound.size());

		booksFound = bookRepository.findByWord("desc");
		assertEquals(4, booksFound.size());
	}

	@Test
	public void addAndFindPublishDateOld() throws RepositoryException {
		ArrayList<Book> books = new ArrayList<>();
		Author author = authorRepository.add(new Author(0, "Daniel Florescu", 1.3333));

		books.add(new Book(
				0,
				"title z'_",
				"subtitle",
				"desc",
				1.3,
				Instant.now(),
				new BigDecimal(1.3),
				author.getId()
		));
		books.add(new Book(
				0,
				"title",
				"subtitle z'_",
				"desc",
				1.3,
				Instant.now(),
				new BigDecimal(1.3),
				author.getId()
		));
		books.add(new Book(
				0,
				"title",
				"subtitle",
				"desc z'_",
				1.3,
				Instant.now(),
				new BigDecimal(1.3),
				author.getId()
		));
		books.add(new Book(
				0,
				"title",
				"subtitle",
				"desc z'_",
				1.3,
				Instant.now().minus(400, ChronoUnit.DAYS),
				new BigDecimal(1.3),
				author.getId()
		));

		for (int n = 0; n < books.size(); n++) {
			books.set(n, bookRepository.add(books.get(n)));
		}

		// Find all books from this year.
		List<Book> booksFound = bookRepository.findByPublicDate(String.format("%s", Instant.now().atZone(ZoneId.of("UTC")).toLocalDate().getYear()));
		assertEquals(3, booksFound.size());
		// Find all books from 3 years ago.
		booksFound = bookRepository.findByPublicDate(String.format("%s", Instant.now().minus(400, ChronoUnit.DAYS).atZone(ZoneId.of("UTC")).toLocalDate().getYear()));
		assertEquals(1, booksFound.size());
	}

	@Test
	public void addAndFindByAuthorIdOld() throws RepositoryException {
		ArrayList<Author> authors = new ArrayList<>();
		ArrayList<Book> books = new ArrayList<>();

		authors.add(new Author(
				0,
				"Daniel Flor",
				1.3
		));
		authors.add(new Author(
				0,
				"Alex Olsen",
				1.1
		));

		for (int n = 0; n < authors.size(); n++) {
			authors.set(n, authorRepository.add(authors.get(n)));
		}

		books.add(new Book(
				0,
				"Title here [[%$å",
				"sub",
				"desc",
				1.3,
				Instant.now(),
				new BigDecimal(1.3),
				authors.get(0).getId()
		));
		books.add(new Book(
				0,
				"Title here [[%$å;'ø",
				"sub",
				"desc",
				1.3,
				Instant.now(),
				new BigDecimal(1.3),
				authors.get(1).getId()
		));
		books.add(new Book(
				0,
				"Title here [[%$å",
				"sub",
				"desc",
				1.3,
				Instant.now(),
				new BigDecimal(1.3),
				authors.get(0).getId()
		));
		books.add(new Book(
				0,
				"Title here [[%$å",
				"sub",
				"desc",
				1.3,
				Instant.now(),
				new BigDecimal(1.3),
				authors.get(0).getId()
		));

		for (int n = 0; n < books.size(); n++) {
			books.set(n, bookRepository.add(books.get(n)));
		}

		List<Book> booksFound = bookRepository.findByAuthorId(authors.get(1).getId());
		assertEquals(1, booksFound.size());
		booksFound = bookRepository.findByAuthorId(authors.get(0).getId());
		assertEquals(3, booksFound.size());
		booksFound = bookRepository.findAll();
		assertEquals(4, booksFound.size());
	}

	@Test
	public void addAndUpdateOld() throws RepositoryException {
		Author author = authorRepository.add(new Author(0, "danie", 69));
		Book book = bookRepository.add(new Book(
				0,
				"title",
				"sub",
				"desc",
				1,
				Instant.now(),
				new BigDecimal(123),
				author.getId()
		));
		book = bookRepository.add(new Book(
				0,
				"title",
				"sub",
				"desc",
				1,
				Instant.now(),
				new BigDecimal(123),
				author.getId()
		));
		book = bookRepository.add(new Book(
				0,
				"title",
				"sub",
				"desc",
				1,
				Instant.now(),
				new BigDecimal(123),
				author.getId()
		));

		book.setTitle("title2");

		Book updatedBook = bookRepository.update(book);

		assertEquals(book.getId(), updatedBook.getId());
		assertEquals(book.getTitle(), updatedBook.getTitle());
	}

	@Test
	public void addAndDeleteOld() throws RepositoryException {
		Author author = authorRepository.add(new Author(0, "da", 6699));
		Book book = bookRepository.add(new Book(
				0,
				"title",
				"sub",
				"desc",
				69,
				Instant.now(),
				new BigDecimal(69),
				author.getId()
		));
		book = bookRepository.add(new Book(
				0,
				"title",
				"sub",
				"desc",
				69,
				Instant.now(),
				new BigDecimal(69),
				author.getId()
		));

		Book foundBook = bookRepository.findById(book.getId()).get();

		bookRepository.deleteById(book.getId());

		assertTrue(bookRepository.findById(book.getId()).isEmpty());
	}

	private boolean bookEqual(@Nullable Book book, @Nullable Book book1) {
		if (book == null && book1 == null) return true;
		else if (book == null || book1 == null) return false;
		else if (book.getId() == book1.getId() &&
				 book.getTitle().equals(book1.getTitle()) &&
				 book.getSubtitle().equals(book1.getSubtitle()) &&
				 book.getDescription().equals(book1.getDescription()) &&
				 Math.abs(book.getRating() - book1.getRating()) < 0.00001 &&
				 book.getPublishDate().equals(book1.getPublishDate()) &&
				 Math.abs(book.getPrice().doubleValue() - book1.getPrice().doubleValue()) < 0.00001 &&
				 book.getAuthorId() == book1.getAuthorId()) {
			return true;
		}
		else return false;
	}

	private void bookAssert(@Nullable Book book, @Nullable Book book1) {
		if (book == null && book1 == null) return;
		if (book == null || book1 == null) assertEquals(book, book1);
		assertEquals(book.getTitle(), book1.getTitle());
		assertEquals(book.getSubtitle(), book1.getSubtitle());
		assertEquals(book.getSubtitle(), book1.getSubtitle());
		assertEquals(book.getDescription(), book1.getDescription());
		assertTrue(Math.abs(book.getRating() - book1.getRating()) <= 0.00001);
		assertEquals(book.getPublishDate(), book1.getPublishDate());
		assertTrue(Math.abs(book.getPrice().doubleValue() - book1.getPrice().doubleValue()) < 0.00001);
		assertEquals(book.getAuthorId(), book1.getAuthorId());
	}

	@Test
	public void addWithInvalidAuthorId() throws RepositoryException {
		boolean caught = false;
		try {
			final Book bookAdded = bookRepository.add(new Book(
					0,
					"title",
					"sub",
					"desc",
					69,
					Instant.now(),
					new BigDecimal(123),
					99
			));
		} catch (RepositoryException e) {
			if (e.getErrorCode() == 5) caught = true;
		}
		assertTrue(caught);
	}

	@Test
	public void addAndGetById() throws RepositoryException {
		Author author = authorRepository.add(new Author(0, "danie", 13));
		ArrayList<Book> books = new ArrayList<>();

		assertTrue(bookRepository.findById(1).isEmpty());

		books.add(new Book(
				0,
				"title",
				"sub",
				"desc",
				69,
				Instant.now(),
				new BigDecimal(123),
				author.getId()
		));
		books.add(new Book(
				0,
				"title",
				"sub",
				"desc",
				69,
				Instant.now(),
				new BigDecimal(123),
				author.getId()
		));
		books.add(new Book(
				0,
				"title",
				"sub",
				"desc",
				69,
				Instant.now(),
				new BigDecimal(123),
				author.getId()
		));

		for (int n = 0; n < books.size(); n++) {
			books.set(n, bookRepository.add(books.get(n)));
			assertNotNull(books.get(n));
			assertNotEquals(0, books.get(n).getId());
		}

		Optional<Book> book = bookRepository.findById(books.get(1).getId());

		assertTrue(book.isPresent());
		bookAssert(books.get(1), book.get());
	}

	@Test
	public void addAndFindByTitle() throws RepositoryException {
		Author author = authorRepository.add(new Author(0, "danie", 13));
		ArrayList<Book> books = new ArrayList<>();

		assertTrue(bookRepository.findByWord("z").isEmpty());

		books.add(new Book(
				0,
				"title",
				"sub <",
				"desc z",
				69,
				Instant.now(),
				new BigDecimal(123),
				author.getId()
		));
		books.add(new Book(
				0,
				"title",
				"sub",
				"desc z",
				69,
				Instant.now(),
				new BigDecimal(123),
				author.getId()
		));
		books.add(new Book(
				0,
				"title",
				"sub",
				"desc z",
				69,
				Instant.now(),
				new BigDecimal(123),
				author.getId()
		));

		for (int n = 0; n < books.size(); n++) {
			books.set(n, bookRepository.add(books.get(n)));
			assertNotNull(books.get(n));
			assertNotEquals(0, books.get(n).getId());
		}

		List<Book> booksFound = bookRepository.findByTitle("z");
		assertEquals(0, booksFound.size());

		books.add(bookRepository.add(new Book(
				0,
				"titzlez",
				"sub",
				"des",
				69,
				Instant.now(),
				new BigDecimal(123),
				author.getId()
		)));

		booksFound = bookRepository.findByTitle("z");
		assertEquals(1, booksFound.size());
		bookAssert(books.get(3), booksFound.get(0));
	}

	@Test
	public void addAndFindByWord() throws RepositoryException {
		Author author = authorRepository.add(new Author(0, "danie", 13));
		ArrayList<Book> books = new ArrayList<>();

		assertTrue(bookRepository.findById(1).isEmpty());

		books.add(new Book(
				0,
				"title",
				"sub",
				"desc",
				69,
				Instant.now(),
				new BigDecimal(123),
				author.getId()
		));
		books.add(new Book(
				0,
				"title",
				"sutrain b",
				"desc",
				69,
				Instant.now(),
				new BigDecimal(123),
				author.getId()
		));
		books.add(new Book(
				0,
				"title",
				"sutrai b",
				"desc",
				69,
				Instant.now(),
				new BigDecimal(123),
				author.getId()
		));

		for (int n = 0; n < books.size(); n++) {
			books.set(n, bookRepository.add(books.get(n)));
			assertNotNull(books.get(n));
			assertNotEquals(0, books.get(n).getId());
		}

		List<Book> booksFound = bookRepository.findByWord("trains");
		assertEquals(0, booksFound.size());
		booksFound = bookRepository.findByWord("train");
		assertEquals(1, booksFound.size());
		bookAssert(books.get(1), booksFound.get(0));
		booksFound = bookRepository.findByWord("trai");
		assertEquals(2, booksFound.size());
	}

	@Test
	public void updateWithInvalidUser() throws RepositoryException {
		Author author = authorRepository.add(new Author(0, "dan", 13));

		boolean caught = false;
		try {
			bookRepository.update(new Book(
					2,
					"title",
					"sutrai b",
					"desc",
					69,
					Instant.now(),
					new BigDecimal(123),
					author.getId()
			));
		} catch (RepositoryException e) {
			assertEquals(3, e.getErrorCode());
			caught = true;
		}
		assertTrue(caught);
	}

	@Test
	public void updateWithInvalidAuthor() throws RepositoryException {
		Author author = authorRepository.add(new Author(0, "da", 69));

		Book book = bookRepository.add(new Book(
				0,
				"title",
				"sutrai b",
				"desc",
				69,
				Instant.now(),
				new BigDecimal(123),
				author.getId()
		));

		boolean caught = false;
		try {
			book.setAuthorId(author.getId() + 1);
			bookRepository.update(book);
		} catch (RepositoryException e) {
			assertEquals(5, e.getErrorCode());
			caught = true;
		}
		assertTrue(caught);
	}

	@Test
	public void deleteWithInvalidUser() throws RepositoryException {
		boolean caught = false;
		try {
			bookRepository.deleteById(1);
		} catch (RepositoryException e) {
			assertEquals(3, e.getErrorCode());
			caught = true;
		}
		assertTrue(caught);
	}
}