package xyz._190405.demo.springbookstore.exception;

import lombok.Getter;
import org.springframework.lang.Nullable;

public class RepositoryException extends Exception {
	private Exception innerException;
	private int errorCode;

	public RepositoryException(@Nullable String message, int errorCode) {
		super(message);
		this.errorCode = errorCode;
	}
	public RepositoryException(@Nullable String message, int errorCode, @Nullable Exception innerException) {
		super(message);
		this.errorCode = errorCode;
		this.innerException = innerException;
	}

	/**
	 * Table of error codes:
	 * 1 - other error
	 * 2 - SQL error
	 * 3 - Not found error (only used in update/delete operations)
	 * 4 - Unique constraint failed (entity already exists).
	 * 5 - Foreign key constraint failed (entity does not exist).
	 * @return error code
	 */
	public int getErrorCode() {
		return errorCode;
	}

	@Override
	public String toString() {
		return String.format("RepositoryException(message=%s, errorCode=%d, innerException=%s)", getMessage(), getErrorCode(), getInnerException());
	}

	@Nullable
	public Exception getInnerException() {
		return innerException;
	}
}
