package xyz._190405.demo.springbookstore.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {
	public void addViewControllers(ViewControllerRegistry registry) {
		// This config is used to tell Spring Security what the pages are called.
		registry.addViewController("/").setViewName("home");
		registry.addViewController("/profile").setViewName("profile");
		registry.addViewController("/book").setViewName("book");
		registry.addViewController("/book/*").setViewName("book");
		registry.addViewController("/login").setViewName("login");
	}
}
