package xyz._190405.demo.springbookstore.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.Nullable;
import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteDataSource;
import xyz._190405.demo.springbookstore.repository.*;

import javax.sql.DataSource;
import javax.validation.constraints.NotNull;
import java.sql.*;
import java.util.ArrayList;

@Configuration
public class DbConfig {
	private static final Logger logger = LoggerFactory.getLogger(DbConfig.class.getName());

	@Value("${jdbc.type}")
	private String dataSourceType = null;
	@Value("${jdbc.url}")
	private String dataSourceUrl = null;
	@Value("${jdbc.autoConfigure}")
	private String dataSourceAutoConfigure = null;

	@NotNull
	private String nullOr(@Nullable String val1, @NotNull String or) {
		if (val1 != null) return val1;
		return or;
	}

	DataSource sqliteDataSourceProvider() {
		// Load shared library.
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (Exception e) {
			String message = e.getMessage();

			if (message.equals("")) message = e.toString();

			logger.error("Caught exception while loading SQLite: %s", message);

			throw new RuntimeException(e);
		}

		// Create config.
		SQLiteConfig config = new SQLiteConfig();
		config.enforceForeignKeys(true);

		// Create dataSource.
		var dataSource = new SQLiteDataSource(config);
		dataSource.setUrl(nullOr(dataSourceUrl, "jdbc:sqlite:database.db"));

		// Test if getting connection works.
		try (Connection connection = dataSource.getConnection()) {
			if (connection == null) throw new RuntimeException("Cannot get connection from dataSource.");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

		// Create tables if configured to do so.
		if (nullOr(dataSourceAutoConfigure, "true").equals("true")) {
			try (Connection connection = dataSource.getConnection();
				 Statement statement = connection.createStatement();) {

				statement.execute(
						"CREATE TABLE IF NOT EXISTS sc_author (" +
						"    id INTEGER PRIMARY KEY," +
						"" +
						"    full_name TEXT NOT NULL," +
						"    rating REAL NOT NULL" +
						");");
				statement.execute(
						"CREATE TABLE IF NOT EXISTS sc_book (" +
						"    id INTEGER PRIMARY KEY," +
						"" +
						"    title TEXT NOT NULL," +
						"    subtitle TEXT NOT NULL," +
						"    description TEXT NOT NULL," +
						"    rating REAL NOT NULL," +
						"    publish_date TEXT NOT NULL," +
						"    price TEXT NOT NULL," +
						"    author INTEGER NOT NULL," +
						"" +
						"    FOREIGN KEY(author) REFERENCES sc_author(id) ON DELETE CASCADE" +
						");");
				statement.execute(
						"CREATE TABLE IF NOT EXISTS sc_user (" +
						"    id INTEGER PRIMARY KEY," +
						"" +
						"    username TEXT NOT NULL UNIQUE," +
						"    password TEXT NOT NULL," +
						"    role TEXT NOT NULL" +
						");");

			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}

		// Check if the correct tables exist.
		try (Connection connection = dataSource.getConnection()) {
			ArrayList<String> tablesToCheck = new ArrayList<>();
			ArrayList<String> missingTables = new ArrayList<>();

			tablesToCheck.add("sc_book");
			tablesToCheck.add("sc_author");
			tablesToCheck.add("sc_user");

			// Check if tables exist.
			for (final String tableName : tablesToCheck) {
				try (PreparedStatement statement = connection.prepareStatement(
						"SELECT name FROM sqlite_master WHERE type = 'table' AND name = ?"
				)) {
					statement.setString(1, "sc_book");
					ResultSet rs = statement.executeQuery();
					if (!rs.next()) missingTables.add(tableName);
				}
			}

			// Throw exception with all the missing tables.
			if (missingTables.size() > 0) {
				StringBuilder sb = new StringBuilder();
				for (final String missingTable : missingTables) {
					sb.append(missingTable);
					sb.append(", ");
				}
				throw new RuntimeException(String.format("Missing these tables in database: %s", sb));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

		// Show happy message.
		logger.info("SQLite dataSource created successfully :)");

		return dataSource;
	}

	@Bean
	DataSource dataSourceProvider() {
		String dbType = nullOr(dataSourceType, "sqlite");

		if (dbType.equalsIgnoreCase("sqlite")) {
			return sqliteDataSourceProvider();
		} else {
			throw new RuntimeException("Unknown database type.");
		}
	}

	@Bean
	AuthorRepository authorRepositoryProvider() {
		String dbType = nullOr(dataSourceType, "sqlite");

		if (dbType.equalsIgnoreCase("sqlite")) {
			return new AuthorRepositorySQLiteImpl();
		} else {
			throw new RuntimeException("Unknown database type.");
		}
	}

	@Bean
	BookRepository bookRepositoryProvider() {
		String dbType = nullOr(dataSourceType, "sqlite");

		if (dbType.equalsIgnoreCase("sqlite")) {
			return new BookRepositorySQLiteImpl();
		} else {
			throw new RuntimeException("Unknown database type.");
		}
	}

	@Bean
	UserRepository userRepositoryProvider() {
		String dbType = nullOr(dataSourceType, "sqlite");

		if (dbType.equalsIgnoreCase("sqlite")) {
			return new UserRepositorySQLiteImpl();
		} else {
			throw new RuntimeException("Unknown database type.");
		}
	}
}
