package xyz._190405.demo.springbookstore.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import xyz._190405.demo.springbookstore.exception.RepositoryException;
import xyz._190405.demo.springbookstore.model.Author;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AuthorRepositorySQLiteImpl implements AuthorRepository {
	private static final Logger logger = LoggerFactory.getLogger(AuthorRepository.class);
	@Autowired
	DataSource dataSource;

	@Override
	public Author add(Author author) throws RepositoryException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO sc_author(rating, full_name) VALUES(?, ?);");
			 PreparedStatement preparedStatement1 = connection.prepareStatement("SELECT last_insert_rowid();")) {
			connection.setAutoCommit(false);

			try {
				preparedStatement.setDouble(1, author.getRating());
				preparedStatement.setString(2, author.getFullName());

				preparedStatement.execute();

				ResultSet rs = preparedStatement1.executeQuery();

				Author returnAuthor = new Author(rs.getLong(1), author.getFullName(), author.getRating());

				connection.commit();

				return returnAuthor;
			} catch (Exception e) {
				connection.rollback();
				throw e;
			}
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage(), 2, e);
		}
	}

	@Override
	public Optional<Author> findById(long id) throws RepositoryException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("SELECT full_name, rating FROM sc_author WHERE id=?;")) {
			preparedStatement.setLong(1, id);

			ResultSet rs = preparedStatement.executeQuery();

			if (!rs.next()) return Optional.empty();
			return Optional.of(new Author(id, rs.getString(1), rs.getDouble(2)));
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage(), 2, e);
		}
	}

	@Override
	public List<Author> findByFullName(String fullName) throws RepositoryException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("SELECT id, full_name, rating FROM sc_author WHERE full_name LIKE ? ESCAPE '$';")) {
			fullName = fullName
					.replace("$", "$$")
					.replace("%", "$%")
					.replace("_", "$_");
			fullName = String.format("%%%s%%", fullName);

			preparedStatement.setString(1, fullName);

			ResultSet rs = preparedStatement.executeQuery();

			ArrayList<Author> authors = new ArrayList<>();
			while (rs.next()) {
				authors.add(new Author(
						rs.getLong(1),
						rs.getString(2),
						rs.getDouble(3)
				));
			}

			return authors;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage(), 2, e);
		}
	}

	@Override
	public List<Author> findAll() throws RepositoryException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("SELECT id, full_name, rating FROM sc_author;")) {
			ResultSet rs = preparedStatement.executeQuery();

			ArrayList<Author> authors = new ArrayList<>();
			while (rs.next()) {
				authors.add(new Author(
						rs.getLong(1),
						rs.getString(2),
						rs.getDouble(3)
				));
			}

			return authors;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage(), 2, e);
		}
	}

	@Override
	public Author update(Author author) throws RepositoryException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("UPDATE sc_author SET full_name=?, rating=? WHERE id=?;")) {
			preparedStatement.setString(1, author.getFullName());
			preparedStatement.setDouble(2, author.getRating());
			preparedStatement.setLong(3, author.getId());

			int rowsUpdated = preparedStatement.executeUpdate();
			if (rowsUpdated != 1) {
				throw new RepositoryException("Author not found in database", 3, null);
			}

			return author;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage(), 2, e);
		}
	}

	@Override
	public void deleteById(long id) throws RepositoryException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM sc_author WHERE id=?;")) {
			preparedStatement.setLong(1, id);

			int rowsUpdated = preparedStatement.executeUpdate();
			if (rowsUpdated != 1) {
				throw new RepositoryException("Author not found in database", 3, null);
			}

		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage(), 2, e);
		}
	}
}
