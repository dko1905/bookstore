package xyz._190405.demo.springbookstore.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.sqlite.SQLiteException;
import xyz._190405.demo.springbookstore.exception.RepositoryException;
import xyz._190405.demo.springbookstore.model.Author;
import xyz._190405.demo.springbookstore.model.User;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserRepositorySQLiteImpl implements UserRepository {
	private static final Logger logger = LoggerFactory.getLogger(UserRepository.class);
	@Autowired
	DataSource dataSource;

	@Override
	public User add(User user) throws RepositoryException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO sc_user(username, password, role) VALUES(?, ?, ?);");
			 PreparedStatement preparedStatement1 = connection.prepareStatement("SELECT last_insert_rowid();")) {
			connection.setAutoCommit(false);

			try {
				preparedStatement.setString(1, user.getUsername());
				preparedStatement.setString(2, user.getPassword());
				preparedStatement.setString(3, user.getRole());

				preparedStatement.execute();

				ResultSet rs = preparedStatement1.executeQuery();

				User returnUser = new User(
						rs.getLong(1),
						user.getUsername(),
						user.getPassword(),
						user.getRole()
				);

				connection.commit();

				return returnUser;
			} catch (SQLException e) {
				connection.rollback();
				if (e.getErrorCode() == 19) {
					logger.warn("UNIQUE constraint failed for {}", user);
					throw new RepositoryException(e.getMessage(), 4, e);
				}
				throw e;
			}
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage(), 2, e);
		}
	}

	@Override
	public Optional<User> findById(long id) throws RepositoryException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("SELECT username, password, role FROM sc_user WHERE id=?;")) {
			preparedStatement.setLong(1, id);

			ResultSet rs = preparedStatement.executeQuery();

			if (!rs.next()) return Optional.empty();
			return Optional.of(new User(
					id,
					rs.getString(1),
					rs.getString(2),
					rs.getString(3)
			));
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage(), 2, e);
		}
	}

	@Override
	public Optional<User> findByUsername(String username) throws RepositoryException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("SELECT id, password, role FROM sc_user WHERE username=?;")) {
			preparedStatement.setString(1, username);

			ResultSet rs = preparedStatement.executeQuery();

			if (!rs.next()) return Optional.empty();
			return Optional.of(new User(
					rs.getLong(1),
					username,
					rs.getString(2),
					rs.getString(3)
			));
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage(), 2, e);
		}
	}

	@Override
	public List<User> findAll() throws RepositoryException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("SELECT id, username, password, role FROM sc_user;")) {
			ResultSet rs = preparedStatement.executeQuery();

			ArrayList<User> users = new ArrayList<>();
			while (rs.next()) {
				users.add(new User(
						rs.getLong(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4)
				));
			}

			return users;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage(), 2, e);
		}
	}

	@Override
	public User update(User user) throws RepositoryException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("UPDATE sc_user SET username=?, password=?, role=? WHERE id=?;")) {
			preparedStatement.setString(1, user.getUsername());
			preparedStatement.setString(2, user.getPassword());
			preparedStatement.setString(3, user.getRole());
			preparedStatement.setLong(4, user.getId());

			int rowsUpdated = preparedStatement.executeUpdate();
			if (rowsUpdated != 1) {
				throw new RepositoryException("User not found in database", 3, null);
			}

			return user;
		} catch (SQLException e) {
			if (e.getErrorCode() == 19) {
				logger.warn("UNIQUE constraint failed for {}", user);
				throw new RepositoryException(e.getMessage(), 4, e);
			}
			throw new RepositoryException(e.getMessage(), 2, e);
		}
	}

	@Override
	public void deleteById(long id) throws RepositoryException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM sc_user WHERE id=?;")) {
			preparedStatement.setLong(1, id);

			int rowsUpdated = preparedStatement.executeUpdate();
			if (rowsUpdated != 1) {
				throw new RepositoryException("User not found in database", 3, null);
			}
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage(), 2, e);
		}
	}
}
