package xyz._190405.demo.springbookstore.repository;

import org.springframework.stereotype.Repository;
import xyz._190405.demo.springbookstore.exception.RepositoryException;
import xyz._190405.demo.springbookstore.model.User;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

/* Available functions:
 * - [C] ADD
 *
 * - [R] GET BY ID
 * - [R] GET BY USERNAME
 * - [R] GET ALL
 *
 * - [U] UPDATE
 *
 * - [D] DELETE BY ID
 */
@Repository
public interface UserRepository {
	/**
	 * Add user to database.
	 * @param user User to add.
	 * @return The added user with a valid id.
	 * @throws RepositoryException
	 */
	@NotNull
	public User add(@NotNull User user) throws RepositoryException;

	/**
	 * Get user from database using id.
	 * @param id Id to use
	 * @return A valid user or nothing.
	 * @throws RepositoryException
	 */
	@NotNull
	public Optional<User> findById(long id) throws RepositoryException;

	/**
	 * Get user from database using username, since username is unique there can only be one user with the username.
	 * @param username Username to use.
	 * @return A valid user or nothing.
	 * @throws RepositoryException
	 */
	@NotNull
	public Optional<User> findByUsername(@NotNull String username) throws RepositoryException;

	/**
	 * Retrieve all users from database.
	 * @return List of all users.
	 * @throws RepositoryException
	 */
	@NotNull
	public List<User> findAll() throws RepositoryException;

	/**
	 * Update user in database.
	 * @param user User that will be used, the id will be used to update.
	 * @return Updated user.
	 * @throws RepositoryException
	 */
	@NotNull
	public User update(@NotNull User user) throws RepositoryException;

	/**
	 * Delete user from database.
	 * @param id Id of user to delete.
	 * @throws RepositoryException
	 */
	public void deleteById(long id) throws RepositoryException;
}
