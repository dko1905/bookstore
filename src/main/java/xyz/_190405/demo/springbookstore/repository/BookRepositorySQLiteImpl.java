package xyz._190405.demo.springbookstore.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import xyz._190405.demo.springbookstore.exception.RepositoryException;
import xyz._190405.demo.springbookstore.model.Author;
import xyz._190405.demo.springbookstore.model.Book;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BookRepositorySQLiteImpl implements BookRepository {
	private static final Logger logger = LoggerFactory.getLogger(BookRepository.class);
	@Autowired
	private DataSource dataSource;

	@Override
	public Book add(Book book) throws RepositoryException {
		try (Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO sc_book(title, subtitle, description, rating, publish_date, price, author) VALUES(?, ?, ?, ?, ?, ?, ?);");
			PreparedStatement preparedStatement1 = connection.prepareStatement("SELECT last_insert_rowid();")) {
			connection.setAutoCommit(false);

			try {
				preparedStatement.setString(1, book.getTitle());
				preparedStatement.setString(2, book.getSubtitle());
				preparedStatement.setString(3, book.getDescription());
				preparedStatement.setDouble(4, book.getRating());
				preparedStatement.setString(5, DateTimeFormatter.ISO_INSTANT.format(book.getPublishDate()));
				preparedStatement.setBigDecimal(6, book.getPrice());
				preparedStatement.setLong(7, book.getAuthorId());

				preparedStatement.execute();

				ResultSet rs = preparedStatement1.executeQuery();

				Book returnBook = new Book(
						rs.getLong(1),
						book.getTitle(),
						book.getSubtitle(),
						book.getDescription(),
						book.getRating(),
						book.getPublishDate(),
						book.getPrice(),
						book.getAuthorId()
				);

				connection.commit();

				return returnBook;
			} catch (Exception e) {
				connection.rollback();
				if (e instanceof SQLException) {
					SQLException ex = (SQLException)e;
					if (ex.getErrorCode() == 19) {
						throw new RepositoryException(ex.getMessage(), 5, ex);
					}
				}
				throw e;
			}
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage(), 2, e);
		}
	}

	@Override
	public Optional<Book> findById(long id) throws RepositoryException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("SELECT title, subtitle, description, rating, publish_date, price, author FROM sc_book WHERE id=?;")) {
			preparedStatement.setLong(1, id);

			ResultSet rs = preparedStatement.executeQuery();

			if (!rs.next()) return Optional.empty();
			return Optional.of(new Book(
					id,
					rs.getString(1),
					rs.getString(2),
					rs.getString(3),
					rs.getDouble(4),
					Instant.parse(rs.getString(5)),
					rs.getBigDecimal(6),
					rs.getLong(7)
			));
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage(), 2, e);
		}
	}

	@Override
	public List<Book> findByTitle(String title) throws RepositoryException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("SELECT id, title, subtitle, description, rating, publish_date, price, author FROM sc_book WHERE title LIKE ? ESCAPE '$';")) {
			title = title
					.replace("$", "$$")
					.replace("%", "$%")
					.replace("_", "$_");
			title = String.format("%%%s%%", title);

			preparedStatement.setString(1, title);

			ResultSet rs = preparedStatement.executeQuery();
			ArrayList<Book> books = new ArrayList<>();

			while (rs.next()) {
				books.add(new Book(
						rs.getLong(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getDouble(5),
						Instant.parse(rs.getString(6)),
						rs.getBigDecimal(7),
						rs.getLong(8)
				));
			}

			return books;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage(), 2, e);
		}
	}

	@Override
	public List<Book> findByWord(String word) throws RepositoryException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement(
			 		"SELECT id, title, subtitle, description, rating, publish_date, price, author FROM sc_book " +
							"WHERE (title LIKE ? ESCAPE '$' " +
							"OR subtitle LIKE ? ESCAPE '$' " +
							"OR description LIKE ? ESCAPE '$');"
			 )) {
			word = word
					.replace("$", "$$")
					.replace("%", "$%")
					.replace("_", "$_");
			word = String.format("%%%s%%", word);

			preparedStatement.setString(1, word);
			preparedStatement.setString(2, word);
			preparedStatement.setString(3, word);

			ResultSet rs = preparedStatement.executeQuery();
			ArrayList<Book> books = new ArrayList<>();

			while (rs.next()) {
				books.add(new Book(
						rs.getLong(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getDouble(5),
						Instant.parse(rs.getString(6)),
						rs.getBigDecimal(7),
						rs.getLong(8)
				));
			}

			return books;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage(), 2, e);
		}
	}

	@Override
	public List<Book> findByPublicDate(String dateLikeStr) throws RepositoryException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("SELECT id, title, subtitle, description, rating, publish_date, price, author FROM sc_book WHERE publish_date LIKE ? ESCAPE '$';")) {
			dateLikeStr = dateLikeStr
					.replace("$", "$$")
					.replace("%", "$%")
					.replace("_", "$_");
			dateLikeStr = String.format("%%%s%%", dateLikeStr);

			preparedStatement.setString(1, dateLikeStr);

			ResultSet rs = preparedStatement.executeQuery();
			ArrayList<Book> books = new ArrayList<>();

			while (rs.next()) {
				books.add(new Book(
						rs.getLong(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getDouble(5),
						Instant.parse(rs.getString(6)),
						rs.getBigDecimal(7),
						rs.getLong(8)
				));
			}

			return books;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage(), 2, e);
		}
	}

	@Override
	public List<Book> findByAuthorId(long authorId) throws RepositoryException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("SELECT id, title, subtitle, description, rating, publish_date, price, author FROM sc_book WHERE author=?;")) {
			preparedStatement.setLong(1, authorId);

			ResultSet rs = preparedStatement.executeQuery();
			ArrayList<Book> books = new ArrayList<>();

			while (rs.next()) {
				books.add(new Book(
						rs.getLong(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getDouble(5),
						Instant.parse(rs.getString(6)),
						rs.getBigDecimal(7),
						rs.getLong(8)
				));
			}

			return books;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage(), 2, e);
		}
	}

	@Override
	public List<Book> findAll() throws RepositoryException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("SELECT id, title, subtitle, description, rating, publish_date, price, author FROM sc_book;")) {
			ResultSet rs = preparedStatement.executeQuery();
			ArrayList<Book> books = new ArrayList<>();

			while (rs.next()) {
				books.add(new Book(
						rs.getLong(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getDouble(5),
						Instant.parse(rs.getString(6)),
						rs.getBigDecimal(7),
						rs.getLong(8)
				));
			}

			return books;
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage(), 2, e);
		}
	}

	@Override
	public Book update(Book book) throws RepositoryException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("UPDATE sc_book SET title=?, subtitle=?, description=?, rating=?, publish_date=?, price=?, author=? WHERE id=?;")) {
			preparedStatement.setString(1, book.getTitle());
			preparedStatement.setString(2, book.getSubtitle());
			preparedStatement.setString(3, book.getDescription());
			preparedStatement.setDouble(4, book.getRating());
			preparedStatement.setString(5, DateTimeFormatter.ISO_INSTANT.format(book.getPublishDate()));
			preparedStatement.setBigDecimal(6, book.getPrice());
			preparedStatement.setLong(7, book.getAuthorId());

			preparedStatement.setLong(8, book.getId());

			int rowsUpdated = preparedStatement.executeUpdate();
			if (rowsUpdated != 1) {
				throw new RepositoryException("Book not found in database", 3, null);
			}

			return book;
		} catch (SQLException e) {
			if (e.getErrorCode() == 19) {
				throw new RepositoryException(e.getMessage(), 5, e);
			}
			throw new RepositoryException(e.getMessage(), 2, e);
		}
	}


	@Override
	public void deleteById(long id) throws RepositoryException {
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM sc_book WHERE id=?;")) {
			preparedStatement.setLong(1, id);

			int rowsUpdated = preparedStatement.executeUpdate();
			if (rowsUpdated != 1) {
				throw new RepositoryException("Book not found in database", 3, null);
			}
		} catch (SQLException e) {
			throw new RepositoryException(e.getMessage(), 2, e);
		}
	}
}
