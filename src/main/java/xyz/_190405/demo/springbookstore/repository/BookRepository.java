package xyz._190405.demo.springbookstore.repository;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;
import xyz._190405.demo.springbookstore.exception.RepositoryException;
import xyz._190405.demo.springbookstore.model.Author;
import xyz._190405.demo.springbookstore.model.Book;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

/* Available functions:
 * - [C] ADD
 *
 * - [R] GET BY ID
 * - [R] GET LIKE TITLE
 * - [R] GET LIKE TITLE,SUBTITLE,DESCRIPTION
 * - [R] GET LIKE PUBLISH_DATE
 * - [R] GET BY AUTHOR
 * - [R] GET ALL
 *
 * - [U] UPDATE
 *
 * - [D] DELETE BY ID
 */
@Repository
public interface BookRepository {
	/**
	 * Add book to database.
	 * @param book Book to add.
	 * @return A valid `Book`.
	 * @throws RepositoryException Error code 5 will be used if invalid author is used.
	 */
	@NotNull
	public Book add(@NotNull Book book) throws RepositoryException;

	/**
	 * Get a book by its id.
	 * @param id Id to search with.
	 * @return A valid `Book` or nothing if it is not found.
	 * @throws RepositoryException
	 */
	@NotNull
	public Optional<Book> findById(long id) throws RepositoryException;

	/**
	 * Find list of books with similar titles.
	 * @param title Title to search with.
	 * @return A `List` of `Book`s, might be 0 in length.
	 * @throws RepositoryException
	 */
	@NotNull
	public List<Book> findByTitle(@NotNull String title) throws RepositoryException;

	/**
	 * Find list of books with the piece of text `word` in it.
	 * @param word Piece of text to search with.
	 * @return List of books that where found, might be 0 in length.
	 * @throws RepositoryException
	 */
	@NotNull
	public List<Book> findByWord(@NotNull String word) throws RepositoryException;

	/**
	 * Find list of books with similar dates, it finds similar dates by using an ISO8601 string.
	 * @param dateLikeString Prefer iso8601 string but can be any substring of it.
	 * @return List of books that where found, might be 0 in length.
	 * @throws RepositoryException
	 */
	@NotNull
	public List<Book> findByPublicDate(@NotNull String dateLikeString) throws RepositoryException;

	/**
	 * Find list of books with same author, it uses the author id.
	 * @param authorId Id of author that you want to search with.
	 * @return A list of books with the author.
	 * @throws RepositoryException
	 */
	@NotNull
	public List<Book> findByAuthorId(long authorId) throws RepositoryException;

	/**
	 * Get all books from database.
	 * @return List of books in database.
	 * @throws RepositoryException
	 */
	@NotNull
	public List<Book> findAll() throws RepositoryException;

	/**
	 * Update book in the database.
	 * @param book Book that will be updated, it will use the id to find the book, and update the rest of the info.
	 * @return A valid book.
	 * @throws RepositoryException Uses error code `3` if the book isn't found and error code `5` if author doesn't
	 * exist.
	 */
	@NotNull
	public Book update(@NotNull Book book) throws RepositoryException;

	/**
	 * Remove book from database.
	 * @param id Id of book to remove.
	 * @throws RepositoryException Uses error code `3` if the book isn't found.
	 */
	public void deleteById(long id) throws RepositoryException;
}
