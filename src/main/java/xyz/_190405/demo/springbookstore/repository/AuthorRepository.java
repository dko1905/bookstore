package xyz._190405.demo.springbookstore.repository;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;
import xyz._190405.demo.springbookstore.exception.RepositoryException;
import xyz._190405.demo.springbookstore.model.Author;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Repository
public interface AuthorRepository {
	/**
	 * Add author to database.
	 * @param author Author to add, the id will not be used.
	 * @return Input author with a valid id.
	 * @throws RepositoryException
	 */
	@NotNull
	public Author add(@NotNull Author author) throws RepositoryException;

	/**
	 * Get author by id.
	 * @param id The id of the author you want to get.
	 * @return `null` if not found, else a valid `Author`.
	 * @throws RepositoryException
	 */
	@NotNull
	public Optional<Author> findById(long id) throws RepositoryException;

	/**
	 * Get all authors with similar name.
	 * @param fullName Name to search with.
	 * @return All authors with similar names in `List`, list size might be `0`.
	 * @throws RepositoryException
	 */
	@NotNull
	public List<Author> findByFullName(@NotNull String fullName) throws RepositoryException;

	/**
	 * Get all authors in database.
	 * @return All authors in a `List`, list size might be `0`.
	 * @throws RepositoryException
	 */
	@NotNull
	public List<Author> findAll() throws RepositoryException;

	/**
	 * Update author in database.
	 * @param author A valid author with a valid id.
	 * @return Updated author.
	 * @throws RepositoryException If any SQL errors or if the author isn't found (error code not found).
	 */
	@NotNull
	public Author update(@NotNull Author author) throws RepositoryException;

	/**
	 * Delete author from database.
	 * @param id Id of author to delete.
	 * @throws RepositoryException
	 */
	public void deleteById(long id) throws RepositoryException;
}
