package xyz._190405.demo.springbookstore.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import xyz._190405.demo.springbookstore.exception.RepositoryException;
import xyz._190405.demo.springbookstore.model.User;
import xyz._190405.demo.springbookstore.repository.UserRepository;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {
	private static Logger logger = LoggerFactory.getLogger(UserDetailsService.class);
	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) {
		try {
			Optional<User> user = userRepository.findByUsername(username);
			if (user.isEmpty()) {
				throw new UsernameNotFoundException(username);
			}
			return new UserPrincipal(user.get());
		} catch (RepositoryException e) {
			logger.warn("loadUserByUsername: Caught repo exception: {}", e.getMessage());
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	public class UserPrincipal implements UserDetails {
		private User user;

		public UserPrincipal(User user) {
			this.user = user;
		}

		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			return Arrays.stream(user.getRole().split(",", 20))
					.map(SimpleGrantedAuthority::new)
					.collect(Collectors.toList());
		}

		@Override
		public String getPassword() {
			return user.getPassword();
		}

		@Override
		public String getUsername() {
			return user.getUsername();
		}

		@Override
		public boolean isAccountNonExpired() {
			return true;
		}

		@Override
		public boolean isAccountNonLocked() {
			return true;
		}

		@Override
		public boolean isCredentialsNonExpired() {
			return true;
		}

		@Override
		public boolean isEnabled() {
			return true;
		}

		public User getUser() {
			return user;
		}
	}
}
