package xyz._190405.demo.springbookstore.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Getter @Setter @AllArgsConstructor @ToString
public class Author {
	private long id;

	private String fullName;
	private double rating;
}
