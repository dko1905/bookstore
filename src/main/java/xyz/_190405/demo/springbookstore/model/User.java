package xyz._190405.demo.springbookstore.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @AllArgsConstructor @ToString
public class User {
	private long id;

	private String username;
	private String password;
	private String role;
}
