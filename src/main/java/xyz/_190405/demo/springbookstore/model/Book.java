package xyz._190405.demo.springbookstore.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.Instant;

@Getter @Setter @AllArgsConstructor @ToString
public class Book {
	private long id;

	private String title;
	private String subtitle;
	private String description;
	private double rating;
	private Instant publishDate;

	private BigDecimal price;

	private long authorId;
}
