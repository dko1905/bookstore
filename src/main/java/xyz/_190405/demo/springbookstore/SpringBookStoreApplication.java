package xyz._190405.demo.springbookstore;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;
import xyz._190405.demo.springbookstore.exception.RepositoryException;
import xyz._190405.demo.springbookstore.model.Author;
import xyz._190405.demo.springbookstore.model.Book;
import xyz._190405.demo.springbookstore.model.User;
import xyz._190405.demo.springbookstore.repository.AuthorRepository;
import xyz._190405.demo.springbookstore.repository.BookRepository;
import xyz._190405.demo.springbookstore.repository.UserRepository;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;

@SpringBootApplication
public class SpringBookStoreApplication implements CommandLineRunner {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	UserRepository userRepository;
	@Autowired
	BookRepository bookRepository;
	@Autowired
	AuthorRepository authorRepository;
	@Autowired
	PasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(SpringBookStoreApplication.class, args);
	}

	@Override
	public void run(String... args) {
		try {
			if (userRepository.findByUsername("daniel").isEmpty()) {
				User user = new User(0, "daniel", passwordEncoder.encode("123"), "USER");
				userRepository.add(user);
			}
		} catch (RepositoryException e) {
			logger.error("Caught: {}", e.toString());
		}
	}
}
