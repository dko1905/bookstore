package xyz._190405.demo.springbookstore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.server.ResponseStatusException;
import xyz._190405.demo.springbookstore.exception.RepositoryException;
import xyz._190405.demo.springbookstore.model.Book;
import xyz._190405.demo.springbookstore.repository.BookRepository;
import xyz._190405.demo.springbookstore.repository.UserRepository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Controller
public class RootController {
	private @Autowired UserRepository userRepository;
	private @Autowired BookRepository bookRepository;
	private @Autowired DataSource dataSource;

	@GetMapping("/")
	public String root(Model model) throws RepositoryException {
		List<Book> books = bookRepository.findAll();
		books =  books.subList(0, Math.min(8, books.size()));

		model.addAttribute("featuredBooks", books);

		return "home";
	}

	@GetMapping("/book")
	public String books(Model model) throws RepositoryException {
		List<Book> books = bookRepository.findAll();

		model.addAttribute("books", books);

		return "books";
	}

	@GetMapping("/book/{id}")
	public String book(Model model, @PathVariable("id") long id) throws RepositoryException {
		Optional<Book> book = bookRepository.findById(id);
		if (book.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Book not found");
		}

		model.addAttribute("book", book.get());

		return "book";
	}
}
